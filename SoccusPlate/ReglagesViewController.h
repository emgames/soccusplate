//
//  ReglagesController.h
//  Soccus
//
//  Created by Alain Robillard on 2015-01-29.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReglagesViewController : UIViewController
- (IBAction)btnInfo:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *viewInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;
@end
