//
//  ViewController.h
//  SoccusPlate
//
//  Created by Alain Robillard on 2015-02-03.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SKUViewController.h"
#import "PanierViewController.h"

@interface ViewController : UIViewController
{
    NSTimer *countdownTimer;                // Timer
    NSTimer *tapToStartTimer;               // Timer
    int countDownInitial;                   // int pour compter
}

- (IBAction)switchPanel:(id)sender;

@property (weak, nonatomic) IBOutlet UISegmentedControl *outSwitchChoix;
@property (weak, nonatomic) IBOutlet UITextField *txtLieu;
@property (weak, nonatomic) IBOutlet UITextField *txtNom;
@property (weak, nonatomic) IBOutlet UIView *viewItemAchete;
@property (weak, nonatomic) IBOutlet UILabel *lblItemAchete;
@property (weak, nonatomic) IBOutlet UIView *containerMesures;
@property (weak, nonatomic) IBOutlet UIView *containerSKU;
@property (weak, nonatomic) IBOutlet UIView *containerPanier;
@property (weak, nonatomic) IBOutlet UIView *containerAccessoires;
@property (weak, nonatomic) IBOutlet UIView *containerRecherche;
@property (weak, nonatomic) IBOutlet UIButton *outTapToStart;
@property (weak, nonatomic) IBOutlet UIImageView *left;
@property (weak, nonatomic) IBOutlet UIImageView *top;
@property (weak, nonatomic) IBOutlet UIImageView *right;
@property (weak, nonatomic) IBOutlet UIImageView *bottom;
@property (weak, nonatomic) IBOutlet UIView *viewTapToStart;

@property(nonatomic) NSMutableArray *mainTablePanier;

- (IBAction)btnRecherche:(id)sender;
- (IBAction)btnTapToStart:(id)sender;

@end

