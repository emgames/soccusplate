//
//  OutilsViewController.m
//  Soccus
//
//  Created by Alain Robillard on 2015-01-29.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import "OutilsViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <AVFoundation/AVFoundation.h>

@interface OutilsViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation OutilsViewController

@synthesize outImport;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)btnImport:(id)sender {
}
- (IBAction)btnExport:(id)sender {
    
    NSMutableString *csv = [NSMutableString stringWithString:@"Modèle; Style; Ccl; Couleur; Pied; Circonférence; Longueur; Grandeur; Quantité; Nom du patient(e); notes"];
    [csv appendFormat:@"\n208;620;1;11;20;2;2;0; 1; Mme France Duquet;"];
    [csv appendFormat:@"\n205;630;0;22;00;3;3;4; 2; Mme Johanne Tremblay; Ne pas livrer le lundi."];
    [csv appendFormat:@"\n262325-1;;;;;;;; 1; Mme Johanne Tremblay;"];
    
    
//    for (Consultation * _entity in _lst)
//    {
//        [csv appendFormat:@"\n%@;%@;%@;%@;%@",
//         _entity.electeur.id_Electeur,
//         [_entity.dateHeureVote WithFormat:@"yyyy-MM-dd HH:mm:ss"],
//         _entity.registre.id_Registre,
//         _entity.electeur.noDepot,
//         _entity.nomOrdinateur
//         ];
//    }
    
    NSString *csvName = @"~/SKU.csv";
    NSString *csvFileName = @"SKU.csv";
    NSDate *csvDate = [NSDate date];

    csvName = [csvName stringByExpandingTildeInPath];
    
    NSError *error;
    BOOL res = [csv writeToFile:csvName atomically:YES encoding:NSISOLatin1StringEncoding error:&error];
    if (!res) {
        NSLog(@"Erreur %@ en écrivant le fichier %@", [error localizedDescription], csvName );
    }
    
    // Lecture des options sur disque

    NSData *csvData = [csv dataUsingEncoding:NSISOLatin1StringEncoding];
    
    MFMailComposeViewController *vc = [[MFMailComposeViewController alloc] init] ;
    vc.mailComposeDelegate = self;
    
    [vc addAttachmentData: csvData mimeType:@"text/csv" fileName:csvFileName];
    
    [vc setSubject:@"Commande avec SKU"];
    [vc setToRecipients: [[NSArray alloc] initWithObjects:@"kpaquet@innovision.qc.ca", nil]];
    [vc setMessageBody: [NSString stringWithFormat: @"Exportation des commandes\nDate et Heure: %@", csvDate] isHTML:NO];
    
    // Present mail view controller on screen
    [self presentViewController:vc animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end

