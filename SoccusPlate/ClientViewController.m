//
//  ClientViewController.m
//  Soccus
//
//  Created by Alain Robillard on 2015-01-29.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import "ClientViewController.h"

@interface ClientViewController ()

@end

@implementation ClientViewController

@synthesize viewTelephone, viewCarteCredit, viewSansPrescrption, outPrivilegie;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [outPrivilegie setImage:[UIImage imageNamed:@"checkbox_unchecked_blanc.png"] forState:UIControlStateNormal];
    [outPrivilegie setImage:[UIImage imageNamed:@"checkbox_checked_blanc.png"] forState:UIControlStateSelected];

}
- (IBAction)switchPanel:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    viewTelephone.hidden = true;
    viewCarteCredit.hidden = true;
    viewSansPrescrption.hidden = true;
    
    switch (selectedSegment) {
        case 0:
            viewTelephone.hidden = false;
            break;
        case 1:
            viewCarteCredit.hidden = false;
            break;
        case 2:
            viewSansPrescrption.hidden = false;
            break;
        default:
            break;
    }
}
- (IBAction)btnPrivilegie:(id)sender {
    outPrivilegie.selected = !outPrivilegie.selected;
}
@end
