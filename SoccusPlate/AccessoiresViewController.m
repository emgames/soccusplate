//
//  AccessoiresViewController.m
//  Soccus
//
//  Created by Alain Robillard on 2015-02-11.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import "AccessoiresViewController.h"

@implementation AccessoiresViewController
@synthesize outPrivilegie, outTableAccessoire;

NSArray *tableAccessoire;
NSMutableArray *tablePanier, *tableAccessoiresChoisis;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    tableAccessoire = @[@"Glider", @"Gants S", @"Gants M", @"Gants L", @"Détergent", @"Recharge Émulsion", @"Lingette"];
    
    tableAccessoiresChoisis = [[NSMutableArray alloc] init];
    [outPrivilegie setImage:[UIImage imageNamed:@"Bouton_PromotionOff.png"] forState:UIControlStateNormal];
    [outPrivilegie setImage:[UIImage imageNamed:@"Bouton_PromotionOn.png"] forState:UIControlStateSelected];
}
- (IBAction)btnPhoto:(id)sender {
}
- (IBAction)btnPrivilegie:(id)sender {
    outPrivilegie.selected = !outPrivilegie.selected;
}

- (IBAction)btnAjouterPanier:(id)sender {

    NSString *contenuPanier = @"";
    
    for (int i=0; i<= tableAccessoiresChoisis.count-1; i++) {
        NSString *item = [tableAccessoiresChoisis objectAtIndex:i];
        if (outPrivilegie.selected){
            item = [NSString stringWithFormat:@"%@ (%@)", item, @"sans frais"];
        }
        [tablePanier addObject:item];
        if (i == 0){
            contenuPanier = [NSString stringWithFormat:@"%@%@", contenuPanier, item];
        }else{
            contenuPanier = [NSString stringWithFormat:@"%@, %@", contenuPanier, item];
        }
    }
    [self afficheAchat:contenuPanier];
    
    for (NSInteger i = tableAccessoiresChoisis.count - 1; i >= 0; i--) {
        [tableAccessoiresChoisis removeObjectAtIndex:i];
    }
    
    for (NSInteger i = tableAccessoire.count - 1; i >= 0; i--) {
        NSIndexPath *path = [NSIndexPath indexPathForItem:i inSection:0];
        UITableViewCell *cell = [outTableAccessoire cellForRowAtIndexPath:path];
        [cell setSelected:NO];
    }
}
-(void) afficheAchat:(NSString*) item{
    
    //    lblItemAchete.text = item;
    //    viewItemAchete.hidden = false;
    //
    //    CABasicAnimation *scaleAnim = [CABasicAnimation animationWithKeyPath:@"transform"];
    //    scaleAnim.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    //    scaleAnim.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)];
    //    scaleAnim.removedOnCompletion = YES;
    //
    //    CABasicAnimation *opacityAnim = [CABasicAnimation animationWithKeyPath:@"alpha"];
    //    opacityAnim.fromValue = [NSNumber numberWithFloat:1.0];
    //    opacityAnim.toValue = [NSNumber numberWithFloat:0.1];
    //    opacityAnim.removedOnCompletion = YES;
    //
    //    UIBezierPath *movePath = [UIBezierPath bezierPath];
    //    CGPoint ctlPoint = CGPointMake(outSwitchChoix.center.x, viewItemAchete.center.y);
    //    [movePath moveToPoint:viewItemAchete.center];
    //    [movePath addQuadCurveToPoint:outSwitchChoix.center
    //                     controlPoint:ctlPoint];
    //
    //    CAKeyframeAnimation *moveAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    //    moveAnim.path = movePath.CGPath;
    //    moveAnim.removedOnCompletion = YES;
    //
    //    CAAnimationGroup *animGroup = [CAAnimationGroup animation];
    //    animGroup.animations = [NSArray arrayWithObjects:moveAnim, scaleAnim, opacityAnim, nil];
    //    animGroup.duration = 1;
    //
    //    [CATransaction begin]; {
    //        [CATransaction setCompletionBlock:^{
    //            viewItemAchete.hidden = true;
    //            [outSwitchChoix setTitle:[NSString stringWithFormat:@"%@ (%lu)", @"Panier", (unsigned long)tablePanier.count] forSegmentAtIndex:2];
    //        }];
    //        [viewItemAchete.layer addAnimation:animGroup forKey:nil];
    //    } [CATransaction commit];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (tableView.tag) {
        case 4:
            return [tableAccessoire count];
            break;
        default:
            return 0;
            break;
    }
    return 0;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    switch (tableView.tag) {
        case 4:
            cell.textLabel.text = [tableAccessoire objectAtIndex:indexPath.row];
            break;
        default:
            cell.textLabel.text = nil;
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (NSInteger i = tableAccessoiresChoisis.count - 1; i >= 0; i--) {
        if ([tableAccessoiresChoisis objectAtIndex:(i)] == [tableAccessoire objectAtIndex:indexPath.row])
        {
            [tableAccessoiresChoisis removeObjectAtIndex:i];
        }
    }

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableAccessoiresChoisis containsObject:[tableAccessoire objectAtIndex:indexPath.row]])
    {
        [tableAccessoiresChoisis removeObject:indexPath];
    }
    else
    {
        [tableAccessoiresChoisis addObject: [tableAccessoire objectAtIndex:indexPath.row]];
    }
}
@end
