//
//  MapViewController.m
//  Soccus
//
//  Created by Alain Robillard on 2015-01-29.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>

@interface MapViewController ()

@end

@implementation MapViewController

@synthesize viewMap;

CLLocationManager *locationManager;

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    

    viewMap.delegate = self;
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = (id)self;
    
    if(IS_OS_8_OR_LATER) {
        [locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    viewMap.showsUserLocation = YES;
    [viewMap setMapType:MKMapTypeStandard];
    [viewMap setZoomEnabled:YES];
    [viewMap setScrollEnabled:YES];
    [viewMap setUserTrackingMode:MKUserTrackingModeFollow animated:YES];

    CLLocationCoordinate2D  ctrpoint;
    ctrpoint.latitude = 46.8152117;
    ctrpoint.longitude = -71.2107848;

    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = ctrpoint;
    point.title = @"Hotel-Dieu de Québec (CHUQ)";
    point.subtitle = @"22 clients, 8 commandes en cours (176 à date)";
    [self.viewMap addAnnotation:point];

    MKPointAnnotation *point2 = [[MKPointAnnotation alloc] init];
    ctrpoint.latitude = 46.7957534;
    ctrpoint.longitude = -71.2522222;
    point2.coordinate = ctrpoint;
    point2.title = @"Hôpital Jeffery Hale";
    point2.subtitle = @"10 clients, 3 commandes en cours (57 à date)";
    [self.viewMap addAnnotation:point2];

    MKPointAnnotation *point3 = [[MKPointAnnotation alloc] init];
    ctrpoint.latitude = 46.7985575;
    ctrpoint.longitude = -71.24563230000001;
    point3.coordinate = ctrpoint;
    point3.title = @"Hôpital du Saint-Sacrement";
    point3.subtitle = @"18 clients, 7 commandes en cours (112 à date)";
    [self.viewMap addAnnotation:point3];
}
@end