//
//  SKUViewController.h
//  Soccus
//
//  Created by Alain Robillard on 2015-02-11.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SKUViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *outBoutonPanier;
@property (weak, nonatomic) IBOutlet UIView *viewBoutons;
@property (weak, nonatomic) IBOutlet UISegmentedControl *outSwitchSKU;
@property (weak, nonatomic) IBOutlet UISegmentedControl *outSwitchChoix;
@property (weak, nonatomic) IBOutlet UIProgressView *barChoix;
@property (weak, nonatomic) IBOutlet UITextField *txtQte;
@property (weak, nonatomic) IBOutlet UIStepper *outStepperQte;

- (IBAction)btnAjouterPanier:(id)sender;
- (IBAction)switchChoix:(id)sender;
- (IBAction)stepperQte:(id)sender;

@end
