//
//  RechercheViewController.m
//  Soccus
//
//  Created by Alain Robillard on 2015-02-11.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import "RechercheViewController.h"

@implementation RechercheViewController

@synthesize lieuChoisi, clientChoisi;

NSArray *tableLieu, *tableClient;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    tableLieu = @[@"Hopital Rimouski", @"Hopital Christ-Roi", @"Hotel-Dieu", @"Clinique Joanie Rochon"];
    tableClient = @[@"Alain Robillard", @"Karine Paquet", @"Isabelle Fortier", @"André Julien", @"Mathieu Robillard", @"Joanie Robillard"];

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (tableView.tag) {
        case 1:
            return [tableLieu count];
            break;
        case 2:
            return [tableClient count];
            break;
        default:
            return 0;
            break;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    switch (tableView.tag) {
        case 1:
            cell.textLabel.text = [tableLieu objectAtIndex:indexPath.row];
            break;
        case 2:
            cell.textLabel.text = [tableClient objectAtIndex:indexPath.row];
            break;
        default:
            cell.textLabel.text = nil;
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (tableView.tag) {
        case 1:
            lieuChoisi = [tableLieu objectAtIndex:indexPath.row];
            break;
        case 2:
            clientChoisi = [tableClient objectAtIndex:indexPath.row];
            break;
        default:
            break;
    }
}
@end
