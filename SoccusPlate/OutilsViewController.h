//
//  OutilsController.h
//  Soccus
//
//  Created by Alain Robillard on 2015-01-29.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface OutilsViewController : UIViewController
- (IBAction)btnImport:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *outImport;
- (IBAction)btnExport:(id)sender;

@end
