//
//  ReglagesViewController.m
//  Soccus
//
//  Created by Alain Robillard on 2015-01-29.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import "ReglagesViewController.h"

@interface ReglagesViewController ()

@end

@implementation ReglagesViewController

@synthesize viewInfo, lblVersion;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSString * appBuildString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString * appVersionString = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    lblVersion.text = [NSString stringWithFormat:@"Version %@ (build %@)", appVersionString, appBuildString];
}
- (IBAction)btnInfo:(id)sender {
    viewInfo.hidden = !viewInfo.hidden;
}
@end

