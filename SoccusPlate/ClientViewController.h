//
//  ClientViewController.h
//  Soccus
//
//  Created by Alain Robillard on 2015-01-29.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClientViewController : UIViewController

- (IBAction)switchPanel:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *viewTelephone;
@property (weak, nonatomic) IBOutlet UIView *viewCarteCredit;
@property (weak, nonatomic) IBOutlet UIView *viewSansPrescrption;
- (IBAction)btnPrivilegie:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *outPrivilegie;

@end
