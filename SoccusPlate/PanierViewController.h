//
//  PanierViewController.h
//  Soccus
//
//  Created by Alain Robillard on 2015-02-11.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PanierViewController : UIViewController

- (IBAction)btnAccepterCommande:(id)sender;
- (IBAction)btnAnnulerCommande:(id)sender;
- (IBAction)btnSquare:(id)sender;
- (IBAction)btnNonTaxable:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *outTablePanier;
@property (weak, nonatomic) IBOutlet UITextField *txtSousTotal;
@property (weak, nonatomic) IBOutlet UITextField *txtTPS;
@property (weak, nonatomic) IBOutlet UITextField *txtTVQ;
@property (weak, nonatomic) IBOutlet UITextField *txtTotal;
@property (weak, nonatomic) IBOutlet UIButton *outNonTaxable;

@end
