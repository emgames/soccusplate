//
//  SKUViewController.m
//  Soccus
//
//  Created by Alain Robillard on 2015-02-11.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import "SKUViewController.h"

@implementation SKUViewController
@synthesize viewBoutons, outSwitchSKU, outSwitchChoix, barChoix, outBoutonPanier, outStepperQte;

NSArray *tableModele, *tableStyle, *tableCompression, *tableCouleur, *tablePied, *tableCirconference, *tableLongueur, *tableGrandeur, *tableAccessoire;
NSArray *tableSKUModele, *tableSKUStyle, *tableSKUCompression, *tableSKUCouleur, *tableSKUPied, *tableSKUCirconference, *tableSKULongueur, *tableSKUGrandeur;

NSMutableArray *tablePanier;

NSInteger posX, posY;
bool effaceApres;

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    tableModele = @[@"Business", @"Discrétion", @"Impuls", @"Micro", @"Micro Balance", @"Soft", @"Sport", @"Ulcertec"];
    tableStyle  = @[@"Collant", @"Cuisse D/S", @"Genou"];
    tableCouleur = @[@"Beige", @"Espresso", @"Java", @"Noir", @"Naturel"];
    tablePied = @[@"Pied ouvert", @"Pied fermé"];
    tableCompression = @[@"20-30", @"30-40", @"40-50"];
    tableCirconference = @[@"Normal", @"Plus"];
    tableLongueur = @[@"Court", @"Long"];
    tableGrandeur = @[@"XSmall", @"Small", @"Medium", @"Large", @"XLarge"];
    tableAccessoire = @[@"Glider", @"Gants S", @"Gants M", @"Gants L", @"Détergent", @"Recharge Émulsion", @"Lingette"];
    
    tablePanier = [[NSMutableArray alloc] init];
    
    tableSKUModele = @[@"208", @"216", @"205", @"218", @"281", @"212", @"218", @"209"];
    tableSKUStyle  = @[@"620", @"630", @"800"];
    tableSKUCouleur = @[@"11", @"22", @"04", @"17", @"00"];
    tableSKUPied = @[@"20", @"00"];
    tableSKUCompression = @[@"1", @"2", @"0"];
    tableSKUCirconference = @[@"2", @"3"];
    tableSKULongueur = @[@"2", @"3"];
    tableSKUGrandeur = @[@"0", @"1", @"2", @"3", @"4"];
    
    effaceApres = false;
    [self nouveauSKU];
}
- (IBAction)btnPhoto:(id)sender {
//    [self.delegate SKUViewControllerWasTapped:self];
}
-(void) nouveauSKU{
    for(int i = 0; i <= 7; i++) {
        [outSwitchChoix setTitle: @"" forSegmentAtIndex:i];
        [outSwitchSKU setTitle:@"" forSegmentAtIndex:i];
    }
    [outSwitchChoix setSelectedSegmentIndex:UISegmentedControlNoSegment];
    [outSwitchSKU setSelectedSegmentIndex:UISegmentedControlNoSegment];
    barChoix.progress = 0.0;
    effaceApres = false;
    [self removeBlock :viewBoutons :0];
    [self ajouteBoutons];
}
-(void) ajouteBoutons{
    
    posX = 10;
    posY = 10;
    
    [self CreeBouton:@"Modèle" :tableModele :1];
}
-(void) CreeBouton :(NSString*) titre :(NSArray*) table :(long) tag  {
    
    UIFont * customFont = [UIFont fontWithName:@"Helvetica" size:32]; //custom font
    
    CGSize labelSize = [titre sizeWithFont:customFont constrainedToSize:CGSizeMake(380, 20) lineBreakMode:NSLineBreakByTruncatingTail];
    
    UILabel *fromLabel = [[UILabel alloc]initWithFrame:CGRectMake(posX, posY, labelSize.width, labelSize.height)];
    fromLabel.text = titre;
    fromLabel.font = customFont;
    fromLabel.numberOfLines = 1;
    fromLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
    fromLabel.adjustsFontSizeToFitWidth = YES;
    fromLabel.adjustsLetterSpacingToFitWidth = YES;
    fromLabel.minimumScaleFactor = 10.0f/12.0f;
    fromLabel.clipsToBounds = YES;
    fromLabel.backgroundColor = [UIColor clearColor];
    fromLabel.textColor = [UIColor whiteColor];
    fromLabel.textAlignment = NSTextAlignmentLeft;
    [self.viewBoutons addSubview:fromLabel];
    
    posY += 40;
    posX = 10;
    int i;
    
    for (NSString *nom in table) {
        
        UIButton *customButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [customButton setTitle:nom forState:UIControlStateNormal];
        [customButton setContentEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
        [customButton setTitleColor:[UIColor colorWithRed:143/255.f green:252/255.f blue:238/255.f alpha:1.0] forState:UIControlStateNormal];
        [customButton setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        [customButton addTarget:self action:@selector(buttonHighlight:) forControlEvents:UIControlEventTouchDown];
        [customButton addTarget:self action:@selector(buttonNormal:) forControlEvents:UIControlEventTouchUpInside];
        [customButton sizeToFit];
        
        if (posX+customButton.frame.size.width+100 > SCREEN_WIDTH){
            posY += 50;
            posX = 10;
        }
        customButton.tag = tag;
        customButton.frame = CGRectMake (posX, posY, customButton.frame.size.width, customButton.frame.size.height);
        [customButton setBackgroundImage:[[UIImage imageNamed:@"bouttonOff.png"]
                                          resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]
                                forState:UIControlStateNormal];
        [customButton setBackgroundImage:[[UIImage imageNamed:@"bouttonOn.png"]
                                          resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]
                                forState:UIControlStateSelected];
        posX =customButton.frame.origin.x+customButton.frame.size.width+10;
        
        [customButton addTarget:self action:@selector(methodToFire:) forControlEvents:UIControlEventTouchUpInside];
        [self.viewBoutons addSubview:customButton];
        i +=1;
    }
}
-(void) buttonHighlight:(UIButton*)sender{
    UIButton *customButton = sender;
    [customButton setBackgroundImage:[[UIImage imageNamed:@"bouttonOn.png"]
                                      resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]
                            forState:UIControlStateNormal];
    [customButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}
-(void) buttonNormal:(UIButton*)sender{
    UIButton *customButton = sender;
    [customButton setBackgroundImage:[[UIImage imageNamed:@"bouttonOff.png"]
                                      resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]
                            forState:UIControlStateNormal];
    [customButton setTitleColor:[UIColor colorWithRed:143/255.f green:252/255.f blue:238/255.f alpha:1.0] forState:UIControlStateNormal];
    
}
-(void) methodToFire:(UIButton*)sender{
    
    UIButton *boutonSelectionne = sender;
    [outSwitchChoix setTitle: boutonSelectionne.titleLabel.text forSegmentAtIndex:boutonSelectionne.tag-1];
    int i;
    i = 0;
    switch (boutonSelectionne.tag) {
        case 1:
            i = [self trouveSKU:tableModele :boutonSelectionne.titleLabel.text];
            [outSwitchSKU setTitle: [tableSKUModele objectAtIndex:i] forSegmentAtIndex:boutonSelectionne.tag-1];
            break;
        case 2:
            i = [self trouveSKU:tableStyle :boutonSelectionne.titleLabel.text];
            [outSwitchSKU setTitle: [tableSKUStyle objectAtIndex:i] forSegmentAtIndex:boutonSelectionne.tag-1];
            break;
        case 3:
            i = [self trouveSKU:tableCompression :boutonSelectionne.titleLabel.text];
            [outSwitchSKU setTitle: [tableSKUCompression objectAtIndex:i] forSegmentAtIndex:boutonSelectionne.tag-1];
            break;
        case 4:
            i = [self trouveSKU:tableCouleur :boutonSelectionne.titleLabel.text];
            [outSwitchSKU setTitle: [tableSKUCouleur objectAtIndex:i] forSegmentAtIndex:boutonSelectionne.tag-1];
            break;
        case 5:
            i = [self trouveSKU:tablePied :boutonSelectionne.titleLabel.text];
            [outSwitchSKU setTitle: [tableSKUPied objectAtIndex:i] forSegmentAtIndex:boutonSelectionne.tag-1];
            break;
        case 6:
            i = [self trouveSKU:tableCirconference :boutonSelectionne.titleLabel.text];
            [outSwitchSKU setTitle: [tableSKUCirconference objectAtIndex:i] forSegmentAtIndex:boutonSelectionne.tag-1];
            break;
        case 7:
            i = [self trouveSKU:tableLongueur :boutonSelectionne.titleLabel.text];
            [outSwitchSKU setTitle: [tableSKULongueur objectAtIndex:i] forSegmentAtIndex:boutonSelectionne.tag-1];
            break;
        case 8:
            i = [self trouveSKU:tableGrandeur :boutonSelectionne.titleLabel.text];
            [outSwitchSKU setTitle: [tableSKUGrandeur objectAtIndex:i] forSegmentAtIndex:boutonSelectionne.tag-1];
            break;
        default:
            break;
    }
    
    [self removeBlock :viewBoutons :boutonSelectionne.tag];
    
    [self calculProgress];
    
    if (!effaceApres){
        [self addTable:boutonSelectionne.tag+1];
    }
}
-(int) trouveSKU :(NSArray*)table :(NSString*) aTrouver{
    
    int i;
    i = 0;
    for (NSString *nomSKU in table) {
        if (nomSKU == aTrouver){
            break;
        }
        i += 1;
    }
    return i;
}
-(void) calculProgress{
    
    int nombreSegment;
    nombreSegment = 0;
    
    for(int i = 0; i <= 7; i++) {
        if ([outSwitchChoix titleForSegmentAtIndex:i].length > 0){
            nombreSegment += 1;
        }
    }
    barChoix.progress = nombreSegment*0.125;
    if (barChoix.progress >= 1){
        outBoutonPanier.hidden = false;
    }else{
        outBoutonPanier.hidden = true;
    }
}
- (IBAction)btnAjouterPanier:(id)sender {
    
    outBoutonPanier.hidden = true;
    
    NSString *contenuPanier = [outSwitchChoix titleForSegmentAtIndex:0];
    
    for(int i = 1; i <= 7; i++) {
        contenuPanier = [NSString stringWithFormat:@"%@, %@", contenuPanier, [outSwitchChoix titleForSegmentAtIndex:i]];
    }
    
    contenuPanier = [NSString stringWithFormat:@"%@ (Qté %.f)", contenuPanier, outStepperQte.value];
    
    [tablePanier addObject:contenuPanier];
    
    [self afficheAchat:contenuPanier];
    [self nouveauSKU];
}
-(void) afficheAchat:(NSString*) item{
//    lblItemAchete.text = item;
//    viewItemAchete.hidden = false;
//    
//    CABasicAnimation *scaleAnim = [CABasicAnimation animationWithKeyPath:@"transform"];
//    scaleAnim.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
//    scaleAnim.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(0.1, 0.1, 1.0)];
//    scaleAnim.removedOnCompletion = YES;
//    
//    CABasicAnimation *opacityAnim = [CABasicAnimation animationWithKeyPath:@"alpha"];
//    opacityAnim.fromValue = [NSNumber numberWithFloat:1.0];
//    opacityAnim.toValue = [NSNumber numberWithFloat:0.1];
//    opacityAnim.removedOnCompletion = YES;
//    
//    UIBezierPath *movePath = [UIBezierPath bezierPath];
//    CGPoint ctlPoint = CGPointMake(outSwitchChoix.center.x, viewItemAchete.center.y);
//    [movePath moveToPoint:viewItemAchete.center];
//    [movePath addQuadCurveToPoint:outSwitchChoix.center
//                     controlPoint:ctlPoint];
//    
//    CAKeyframeAnimation *moveAnim = [CAKeyframeAnimation animationWithKeyPath:@"position"];
//    moveAnim.path = movePath.CGPath;
//    moveAnim.removedOnCompletion = YES;
//    
//    CAAnimationGroup *animGroup = [CAAnimationGroup animation];
//    animGroup.animations = [NSArray arrayWithObjects:moveAnim, scaleAnim, opacityAnim, nil];
//    animGroup.duration = 1;
//    
//    [CATransaction begin]; {
//        [CATransaction setCompletionBlock:^{
//            viewItemAchete.hidden = true;
//            [outSwitchChoix setTitle:[NSString stringWithFormat:@"%@ (%lu)", @"Panier", (unsigned long)tablePanier.count] forSegmentAtIndex:2];
//        }];
//        [viewItemAchete.layer addAnimation:animGroup forKey:nil];
//    } [CATransaction commit];
}
-(void) addTable:(long)tagaCreer {
    posX = 10;
    posY = 10;
    
    switch (tagaCreer) {
        case 1:
            [self CreeBouton:@"Modèle" :tableModele :tagaCreer];
            break;
        case 2:
            [self CreeBouton:@"Style" :tableStyle :tagaCreer];
            break;
        case 3:
            [self CreeBouton:@"Compression" :tableCompression :tagaCreer];
            break;
        case 4:
            [self CreeBouton:@"Couleur" :tableCouleur :tagaCreer];
            break;
        case 5:
            [self CreeBouton:@"Pied" :tablePied :tagaCreer];
            break;
        case 6:
            [self CreeBouton:@"Circonférence" :tableCirconference :tagaCreer];
            break;
        case 7:
            [self CreeBouton:@"Longueur" :tableLongueur :tagaCreer];
            break;
        case 8:
            [self CreeBouton:@"Grandeur" :tableGrandeur :tagaCreer];
            break;
        default:
            break;
    }
}

- (IBAction)switchChoix:(id)sender {
    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    [self removeBlock :viewBoutons :selectedSegment];
    [self addTable:selectedSegment+1];
    effaceApres = true;
}

- (IBAction)stepperQte:(id)sender {
    self.txtQte.text = [NSString stringWithFormat:@"%.f",outStepperQte.value];
}

-(void) removeBlock:(UIView*)viewADetruire :(long)tagaDetruire{
    for (UIButton *bouton in [viewADetruire subviews])
    {
        [bouton removeFromSuperview];
    }
    for (UILabel *label in [viewADetruire subviews])
    {
        [label removeFromSuperview];
    }
}
@end
