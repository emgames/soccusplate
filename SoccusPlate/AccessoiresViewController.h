//
//  AccessoiresViewController.h
//  Soccus
//
//  Created by Alain Robillard on 2015-02-11.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccessoiresViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *outPrivilegie;
@property (weak, nonatomic) IBOutlet UITableView *outTableAccessoire;

- (IBAction)btnPrivilegie:(id)sender;
- (IBAction)btnAjouterPanier:(id)sender;
@end
