//
//  ViewController.m
//  SoccusPlate
//
//  Created by Alain Robillard on 2015-02-03.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import "ViewController.h"
#import "RechercheViewController.h"
#import "SKUViewController.h"
#import "PanierViewController.h"

@interface ViewController ()

@end

@implementation ViewController
{
    BOOL tapToStart;
}

@synthesize txtLieu, txtNom, mainTablePanier, outTapToStart;

@synthesize containerAccessoires, containerMesures, containerPanier, containerSKU, containerRecherche, top, left, right, bottom, viewTapToStart;

#define SCREEN_WIDTH ((([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortrait) || ([UIApplication sharedApplication].statusBarOrientation == UIInterfaceOrientationPortraitUpsideDown)) ? [[UIScreen mainScreen] bounds].size.width : [[UIScreen mainScreen] bounds].size.height)

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    tapToStartTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerTapToStart) userInfo:nil repeats:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)switchPanel:(id)sender {
//    UISegmentedControl *segmentedControl = (UISegmentedControl *) sender;
    [self afficheBonPanel:sender];
}
-(void) afficheBonPanel:(UISegmentedControl*) controleAAfficher{
    UISegmentedControl *segmentedControl = (UISegmentedControl *) controleAAfficher;
    
    NSInteger selectedSegment = segmentedControl.selectedSegmentIndex;
    
    containerMesures.hidden = true;
    containerSKU.hidden = true;
    containerPanier.hidden = true;
    containerAccessoires.hidden = true;
    
    switch (selectedSegment) {
        case 0:
            containerMesures.hidden = false;
            break;
        case 1:
            containerSKU.hidden = false;
            break;
        case 2:
            containerPanier.hidden = false;
            break;
        case 3:
            containerAccessoires.hidden = false;
            break;
        default:
            break;
    }
}

- (IBAction)btnRecherche:(id)sender {
    containerRecherche.hidden = !containerRecherche.hidden;
}

- (IBAction)btnTapToStart:(id)sender {
    
    outTapToStart.hidden = true;
    [UIView animateWithDuration:2.0f
                          delay:1.0f
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CATransform3D t = CATransform3DIdentity;
                         t = CATransform3DScale(t, -1, -1, -1);
                         
//                         [UIView beginAnimations:nil context:NULL];
//                         [UIView setAnimationDuration:1.5];
                         
                         self.top.layer.transform = t;
                         self.left.layer.transform = t;
                         self.right.layer.transform = t;
                         self.bottom.layer.transform = t;
                         
                         [UIView commitAnimations];
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                             viewTapToStart.hidden = true;
                         }
                     }];
}
- (void)RechercheViewControllerWasTapped:(RechercheViewController *)controller
{
    // do something here like refreshing the table or whatever
    
    // close the delegated view
    txtLieu.text = [controller lieuChoisi];
    txtNom.text = [controller clientChoisi];
    containerRecherche.hidden = true;
}
-(void) timerTapToStart{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:1.0];
    if (tapToStart){
        outTapToStart.alpha = 1.0;
        tapToStart = false;
    }else {
        outTapToStart.alpha = 0.0;
        tapToStart = true;
    }
    [UIView commitAnimations];
}
@end
