//
//  PanierViewController.m
//  Soccus
//
//  Created by Alain Robillard on 2015-02-11.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import "PanierViewController.h"

@implementation PanierViewController
@synthesize outTablePanier, txtSousTotal, txtTotal, txtTPS, txtTVQ, outNonTaxable;

NSMutableArray *tablePanier;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.

    outTablePanier.editing=YES;
    [outNonTaxable setImage:[UIImage imageNamed:@"Bouton_TaxableOff.png"] forState:UIControlStateNormal];
    [outNonTaxable setImage:[UIImage imageNamed:@"Bouton_TaxableOn.png"] forState:UIControlStateSelected];
}

-(void) afficherPanierFromSegue{
    [self.outTablePanier reloadData];
    [self calculTotaux];
}
- (IBAction)btnPhoto:(id)sender {
}
- (IBAction)btnAccepterCommande:(id)sender {
    [self afficherPanierFromSegue];
}

- (IBAction)btnAnnulerCommande:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Annulation d'une commande"
                          message:[NSString stringWithFormat: @"Êtes-vous certain de vouloir Détruire cette commande?"]
                          delegate:self
                          cancelButtonTitle:@"Non"
                          otherButtonTitles:@"Oui", nil];
    [alert show];
}

- (IBAction)btnSquare:(id)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = txtTotal.text;
}

- (IBAction)btnNonTaxable:(id)sender {
    outNonTaxable.selected = !outNonTaxable.selected;
    [self calculTotaux];
}
-(void) calculTotaux{
        txtSousTotal.text = [NSString stringWithFormat:@"%.2f $", (tablePanier.count * 100.0)];
    if (outNonTaxable.selected){
        txtTPS.text = @"0.00 $";
        txtTVQ.text = @"0.00 $";
        txtTotal.text = txtSousTotal.text;
    }else{
        txtTPS.text = [NSString stringWithFormat:@"%.2f $", (tablePanier.count * 100.00) * 0.05];
        txtTVQ.text = [NSString stringWithFormat:@"%.2f $", (tablePanier.count * 100.00) * 0.0975];
        txtTotal.text = [NSString stringWithFormat:@"%.2f $", (tablePanier.count * 100.00) * 1.14975];
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (tableView.tag) {
        case 3:
            return [tablePanier count];
            break;
        default:
            return 0;
            break;
    }
    return 0;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(editingStyle == UITableViewCellEditingStyleDelete & tableView.tag == 3)
    {
        [tablePanier removeObjectAtIndex:indexPath.row];
        [self.outTablePanier reloadData];
        [self calculTotaux];
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    switch (tableView.tag) {
        case 3:
            cell.textLabel.text = [tablePanier objectAtIndex:indexPath.row];
            break;
        default:
            cell.textLabel.text = nil;
            break;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}
@end
