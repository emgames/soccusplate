//
//  RechercheViewController.h
//  Soccus
//
//  Created by Alain Robillard on 2015-02-11.
//  Copyright (c) 2015 Atleon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RechercheViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) NSString* lieuChoisi;
@property (nonatomic, weak) NSString* clientChoisi;

@end
